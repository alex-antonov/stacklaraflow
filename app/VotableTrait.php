<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 1/26/19
 * Time: 8:34 PM
 */

namespace App;


trait VotableTrait
{
    public function votes()
    {
        return $this->morphToMany(User::class, 'votable');
    }

    public function upVotes()
    {
        return $this->votes()->wherePivot('vote', 1);
    }

    public function downVotes()
    {
        return $this->votes()->wherePivot('vote', -1);
    }
}